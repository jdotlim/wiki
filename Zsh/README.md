# Zsh Startup

## Startup Files

1. /etc/zshenv
2. ~/.zshenv
3. /etc/zprofile
4. ~/.zprofile

### if interactive

5. /etc/zshrc
6. ~/.zshrc
7. /etc/login
8. ~/.zlogin

### if non-interactive

5. /etc/login
6. ~/.zlogin

### explicit `logout` or `exit` or implicit end-of-file from terminal
1. /etc/logout
2. ~/.zlogout

### sh mode and login mode

1. /etc/profile
2. ~/.profile
3. the ENV environment variable, if it is set
