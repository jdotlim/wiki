# Writing Plugins

## How to create a mapping that a user can map keys to?

A plugin can create mappings that users can create key maps for. This is useful
if the plugin creates a mapping, but wants to allow the user to pick their own
mapping to call that mapping using recursive mapping. The basic form is:

```
noremap <unique> <script> <Plug>ScriptnameMapname; Action
```

The important part is the `<Plug>`. This is a special code that a typed key
will never produce so it is unlikely that the mapping will already exist. The
`ScriptnameMapname;` is an arbitrary string that given that form by convention
to try and reduce the possibility of naming collisions. Then a user can create
a mapping like this:

```
nmap _p <Plug>ScriptnameMapname
```

Now the keystrokes `_p` in normal mode will invoke the
`<Plug>ScriptnameMapname` mapping.

## How to avoid mapping and function collisions between scripts?

It is possible that mappings and functions create in one script have the same
name as mappings and functions in another script. To uniquely identify these
in a script, we can use `<SID>` Vim will replace `"<SID>"` with the special key
code `<SNR>` followed by a number that's unique for the script, and an
underscore. We could use script local functions to avoid the name collision,
but then Vim would not know where the function came from if it was executed
outside the script context.

A mapping could be written as such:

```
noremap <unique> aa <SID>Add
```

Then `aa` would invoke the `Add()` function defined in the script.

## How to use recursive mapping only for script-local mappings?

A map command can be restricted to only recursively map mappings local to the
script.

```
noremap <SID>FindTopic /Topic<CR>
nmap <script> ,dt <SID>FindTopicdd
```

Then `,dt` will only recursively map `<SID>FindTopic` and not `dd`.
