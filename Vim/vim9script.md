# Vim9 Script

All Vim9 scripts must start with `vim9script`. It is possible to mix legacy
script and Vim9 script, but `vim9script` must appear before any Vim9 syntax.

## Functions

Functions are opened with `def` and closed with `enddef`. Functions must
declare their argument types and return type.

```
def MyFunction(a: number b: string): void
...
enddef
```
