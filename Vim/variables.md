# Variables

## Namespaces

Vim has several different namespaces for variables which are used by prepending
the namespace to the variable. The details are in `:h variable-scope`.
They are summarized here.

- b: Local to the current buffer
- w: Local to the current window
- t: Local to the current tab page
- g: Global
- l: Local to a function (for legacy function)
- s: Local to a :source'ed Vim script
- a: Function argument (for legacy function)
- v: Global, predefined by Vim

A variable with nothing prepended is local to a function inside a function,
global in a legacy script and local to the script in a Vim9 script.
