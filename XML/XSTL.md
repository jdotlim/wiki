# XSLT - eXstensible Stylesheet Language Transformations

## What is XSLT?

A language designed to manipulate XML documents. XSLT itself is XML.

## Hello World

```
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
...
</xsl:stylesheet>
```

**The namespace cannot use https instead of http**
