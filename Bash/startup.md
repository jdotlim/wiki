# Bash Startup

## Startup Files

### interactive login shell / `--login`

1. /etc/profile
2. ~/.bash\_profile > ~/.bash\_login > ~/.profile

There is no error if a file doesn't exist or is not readable.
When an interactive login shell exists or a non-interactive login shell
executes the `exit` command, Bash will source ~/.bash\_logout

### interactive non-login

1. ~/.bashrc

### non-interacitve without `--login`

1. interpret `BASH_ENV` as a file path and source it (if it is set)

### with name `sh` as interactive / `--login`

1. /etc/profile
2. ~/.profile

### with name `sh` as non-interactive

1. none
