# Function Pointers

## Function Decay

A function `f` without a following `(` decays to a pointer to its start.

```
// The type is a function called atexit_function that takes nothing and returns
// nothing.
typedef void atexit_function(void);

// Two equivalent definitions of the same type.
typedef atexit_function* atexit_function_pointer;
//      |                |- name of the new type
//      |- function pointer
typedef void (*atexit_function_pointer)(void);
//            |- writing a function pointer type

// Five equivalent declarations for the same function
void atexit(void f(void));
void atexit(void (*f)(void));
void atexit(atexit_function f);
void atexit(atexit_function* f);
void atexit(atexit_function_pointer f);
```

Technically, pointer decay is always performed and the function is called via a
function pointer.
