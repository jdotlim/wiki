# Git Tips

## Use an external command for diffs

Git can be configured to display diffs using an external tool.

```
git config --global diff.tool vimdiff
git config --global difftool.prompt false
```

This will set git to use vimdiff to show diffs. `diff.tool vimdiff` actually
sets the tool and `difftool.prompt false` will make git show the diffs without
asking first. To use this feature, one must invoke `git difftool`
rather than `git diff` because the former will open one diff at a time while
the latter will write all diffs to stdout or a pager. To get to the next diff
when using `difftool`, simply close vim with `:qa`. `difftool` will open all
files in readonly mode so any changes must be written with `:w!`. To open
editable files, set the `difftool` command.

```
git config --global difftool.vimdiff.cmd 'vimdiff "$LOCAL" "$REMOTE"'
```
